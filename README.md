Puppet Configuration Editor
==============

[![Join the chat at https://gitter.im/tropyx/NetBeansPuppet](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/tropyx/NetBeansPuppet?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)


This is a module for editing Puppet configurations.  It identifies a puppet config tree by the presence of a manifests/site.pp or manifests/init.pp file.

This lets you use IDE tools like inbuilt git/subversion/mercurial support etc. to edit your puppet files.


## Features include:

[Editor](https://github.com/tropyx/NetBeansPuppet/wiki/Editing-Puppet-files) - Puppet file coloring, Editor tabs with module name included

[Navigation](https://github.com/tropyx/NetBeansPuppet/wiki/Navigating-Puppet-files) - Go to Type, Go to Symbol include results from open puppet projects

[Code completion](https://github.com/tropyx/NetBeansPuppet/wiki/Code-completion-in-editor) - Complete variables, functions, resource types

[Hyperlinking](https://github.com/tropyx/NetBeansPuppet/wiki/Editing-Puppet-files) to variable, class definitions

[Puppet lint errors/warnings](https://github.com/tropyx/NetBeansPuppet/wiki/Puppet-Lint-errors-and-fixes) in the editor, configuration read from Rakefile

[Projects](https://github.com/tropyx/NetBeansPuppet/wiki/Puppet-Projects) - Create new Puppet module from template, recognize existing projects by manifests/site.pp or manifests/init.pp presence.

[Where used](https://github.com/tropyx/NetBeansPuppet/wiki/Where-Used-query) - Find usages for variables, class references, functions

* Improved project support (Find, Select in projects work, version control actions on project popup)
* .erb files have mimetype (coloring etc) based on previous extension (.sh.erb/yaml.erb/..)
* Hiera eyaml on-the-fly decrypting
* Works with NetBeans 8.0+


## How To Install:
Download the latest release [nbpuppet-2.0.3](https://github.com/tropyx/NetBeansPuppet/releases/tag/v2.0.3) to your local machine

add it to your NetBeans IDE with:

Tools -> Plugins -> Downloaded -> Add Plugins...

PLEASE NOTE: The latest version of the plugin is currently not available on NetBeans update centers for 8.0 and 8.1. It's listed on the [NetBeans Plugin portal](http://plugins.netbeans.org/plugin/60170/?show=true)


![demo](https://raw.githubusercontent.com/tropyx/NetBeansPuppet/master/screenshot-puppetfornetbeans.png)
Screenshot showing the puppet manifest tree, pp files, and create file dialog box

##License details
Copyright (C) Tropyx Technology Pty Ltd and Michael Lindner Febuary 20 2013

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
